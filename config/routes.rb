Rails.application.routes.draw do
  resources :addresses
  resources :communication_details
  resources :contacts
  root to: 'visitors#index'
end
