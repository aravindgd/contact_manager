class CommunicationDetail < ActiveRecord::Base
  belongs_to :contact, inverse_of: :communication_details
  validates :contact, presence: true

end
