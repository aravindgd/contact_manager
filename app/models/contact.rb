class Contact < ActiveRecord::Base
  has_many :addresses, inverse_of: :contact
  has_many :communication_details, inverse_of: :contact
  accepts_nested_attributes_for :addresses, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :communication_details, reject_if: :all_blank, allow_destroy: true
  
  def name
    self.first_name || "" + self.last_name || ""
  end

end
