class Address < ActiveRecord::Base
  belongs_to :contact, inverse_of: :addresses
  validates :contact, presence: true
  def primary_address
    (self.address_line1 || "") + "-" + (self.country || "")
  end
end
