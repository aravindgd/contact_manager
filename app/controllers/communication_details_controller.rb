class CommunicationDetailsController < ApplicationController
  before_action :set_communication_detail, only: [:show, :edit, :update, :destroy]

  # GET /communication_details
  # GET /communication_details.json
  def index
    @communication_details = CommunicationDetail.all
  end

  # GET /communication_details/1
  # GET /communication_details/1.json
  def show
  end

  # GET /communication_details/new
  def new
    @communication_detail = CommunicationDetail.new
  end

  # GET /communication_details/1/edit
  def edit
  end

  # POST /communication_details
  # POST /communication_details.json
  def create
    @communication_detail = CommunicationDetail.new(communication_detail_params)

    respond_to do |format|
      if @communication_detail.save
        format.html { redirect_to @communication_detail, notice: 'Communication detail was successfully created.' }
        format.json { render :show, status: :created, location: @communication_detail }
      else
        format.html { render :new }
        format.json { render json: @communication_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /communication_details/1
  # PATCH/PUT /communication_details/1.json
  def update
    respond_to do |format|
      if @communication_detail.update(communication_detail_params)
        format.html { redirect_to @communication_detail, notice: 'Communication detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @communication_detail }
      else
        format.html { render :edit }
        format.json { render json: @communication_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /communication_details/1
  # DELETE /communication_details/1.json
  def destroy
    @communication_detail.destroy
    respond_to do |format|
      format.html { redirect_to communication_details_url, notice: 'Communication detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_communication_detail
      @communication_detail = CommunicationDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def communication_detail_params
      params.require(:communication_detail).permit(:phone, :contact_id)
    end
end
