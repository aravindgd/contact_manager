Contanct Manager
================

Ruby on Rails
-------------

This application requires:

- Ruby 2.2.0
- Rails 4.2.2

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).

Getting Started
---------------

- Used Rails App Composer to generate rails skeleton
- Contact model has first name, last name and email
- Address has address_line1 and contact reference
- Communication Details has phone and contact referenece
- Primary phone and primary address is based on the first created Address and Communication Details

Documentation and Support
-------------------------

Issues
-------------

Similar Projects
----------------

Contributing
------------

Credits
-------

License
-------
