class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :address_line1
      t.string :country
      t.belongs_to :contact, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
