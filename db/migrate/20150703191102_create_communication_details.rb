class CreateCommunicationDetails < ActiveRecord::Migration
  def change
    create_table :communication_details do |t|
      t.string :phone
      t.belongs_to :contact, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
